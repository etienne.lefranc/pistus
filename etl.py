"""The ETL module.

Look at the instructions after the statement if __name__ == "__main__":

* First, we extract the data from the input CSV files into a collection of Pandas dataframes.
  Each dataframe corresponds to a table in the target relational database.
* Then, we transform the data in the dataframes.
* Finally, we load the data into the database.

"""

import pandas as pd
import sqlite3
import os
import db
import utils

from datetime import datetime


def extract():
    """Implementation of the extraction submodule.

    Returns
    -------
    dictionary
        The collection of dataframes containing the data of the input CSV files.
        You should have as many dataframes as tables in your relational database.
        Each dataframe corresponds to a table in the relational database.
        The dictionary contains a set of key-value pairs where
            * the value is a dataframe. 
            * the key is the name of the table corresponding to the dataframe  (e.g., "Student", "EmailAddress"...)
            
    """

    # This is the dictonary containing the collection of dataframes.
    # Each item of this dictionary is a key-value pair; the key is the name of a database table;
    # the value is a Pandas dataframe with the content of the table.
    dataframes = {}

    print("Extracting the data from the input CSV files...")

    ################## TODO: COMPLETE THE CODE OF THIS FUNCTION  #####################
    
    input_df_1 = pd.read_csv("./data/student_memberships.csv", delimiter=';')
    input_df_2 = pd.read_csv("./data/student_registrations.csv", delimiter=';')
    dataframes["student_memberships"] = input_df_1
    dataframes["student_registrations"] = input_df_2

    
    
    ##################################################################################

    # Return the dataframe collection.
    return dataframes
    
def transform(dataframes):
    """Implementation of the transformation submodule.

    Parameters
    ----------
    dataframes : dictionary
        This is the dictionary returned by the function load()
    
    Returns 
    -------
    The input dictionary (after the transformations).
    """

    print("Transforming the data...")

    ################## TODO: COMPLETE THE CODE OF THIS FUNCTION  #####################

    
    ##################################################################################

    # Returns the dataframe collection after the transformations.
    return dataframes

def load():
    """Implementation of the load submodule.

    Parameters:
    ----------
    dataframes : dictionary
        The dictionary returned by the function extract()
    """
    # Loads the application configuration.
    app_config = utils.load_config()

    # Gets the path to the database file.
    database_file = app_config["db"]

    # You might bump into some errors while debugging your code which 
    # This might result in a database that is partially filled with some data.
    # Each time you rerun the ETL module, you want the database to be in the same state as when
    # you first created. 
    # The simpler solution here is to remove the database and recreate the tables back again.
    if os.path.exists(database_file):
        # If the database file already exists, we remove it.
        # In order to test the existence of a file, and to remove it, we use functions that are 
        # available in a Python module called "os".
        os.remove(database_file)
    
    # We open a connection to the database.
    conn = sqlite3.connect(database_file)
    dict = extract()
    # We get the cursor to query the database.
    cursor = conn.cursor()

    # We create the tables in the database, by using the function create_database that you implemented in the module
    # db.
    db.create_database(conn, cursor)

    print("Loading the data into the database...")
    
    ################## TODO: COMPLETE THE CODE OF THIS FUNCTION  #####################
    student_mem = dict["student_memberships"]
    student_regi = dict["student_registrations"]
    student_1 = student_mem[["stud_number", "first_name","last_name", "gender"]]
    find_replace = {
    "garçon": "M",
    "fille": "F",
    "homme" : "M",
    "femme" : "F",
    "H" : "M",
    "W" : "F"
    }
    columns_to_replace = {
    "gender": find_replace
    }
    student_1 = student_1.replace(columns_to_replace)
    student_1 = student_1.drop_duplicates()
    student_1.to_sql('Student', conn, if_exists="append", index=False)
    asso = student_mem[["asso_name","asso_desc"]]
    asso = asso.drop_duplicates()
    asso.to_sql('Association', conn, if_exists = "append", index= False)
    member_of = student_mem[["stud_number","asso_name","stud_role"]]
    member_of.to_sql("Member_of", conn, if_exists = "append", index = False)
    register_for = student_regi[["year","stud_number","registration_date","payment_date"]]
    register_for.to_sql("Register_for", conn, if_exists = "append", index = False)
    email = student_regi[["email","stud_number"]]
    email = email.drop_duplicates()
    email.to_sql("Email_address", conn, if_exists = "append", index = False)
    Pistus_Edition = student_regi[["year","registration_fee"]]
    Pistus_Edition = Pistus_Edition.drop_duplicates()
    Pistus_Edition.to_sql("Pistus_Edition",conn, if_exists = "append", index = False)

    ##################################################################################
    print("Done!")
    
    # We close the connection to the database.
    cursor.close()
    conn.close()
print(load())
# Entry point of the ETL module.
if __name__ == "__main__":

    ################## TODO: COMPLETE THE CODE OF THIS FUNCTION  #####################
    dict = extract()
    #print(dict["student_memberships"])
    #print(dict["student_registrations"])

    
    ##################################################################################
    